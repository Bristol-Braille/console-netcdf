# Real example netCDF files:
# https://www.unidata.ucar.edu/software/netcdf/examples/files.html

# Reference URLs for manipulating files:
# https://opensourceoptions.com/intro-to-netcdf-with-python-netcdf4/
# https://opensourceoptions.com/netcdf-with-python-netcdf4-metadata-dimensions-and-variables/
# https://unidata.github.io/netcdf4-python/

import netCDF4 as nc
import os

os.system('clear')

fn = 'sresa1b_ncar_ccsm3-example.nc' # path to netcdf file
ds = nc.Dataset(fn)  # read as netcdf dataset

print("Example data from the Community")
print("Climate System Model (CCSM), one")
print("time step of precipitation flux,")
print("air temperature, and eastward")
print("wind. Intended as a demo script")
print("showing some methods, not as a")
print("functional tool. N.B. To scroll")
print("press ctrl+shift+up/down arrows")

user_choice = input('See dictionary of dataset? (y/n)')
if user_choice.lower() == 'y':
	for key, value in ds.__dict__.items():
		print(key, ': ', value, end='')
		input()

user_choice = input('See lists of variables? (y/n)')
if user_choice.lower() == 'y':
	for var in ds.variables.values():
		print(var, end='')
		input()

user_choice = input('Type variable to list (e.g. lat):')
if user_choice != '':
	os.system('clear')
	user_choice_var = ds[user_choice][:]
	input('Press enter to scan range of '+user_choice)
	os.system('clear')
	for i in range(len(user_choice_var)):
		print(user_choice_var[i], end='')
		input()
		if not i % 8 and i > 0:
			os.system('clear')
	os.system('clear')
	input('Press enter to see full range of '+user_choice)
	os.system('clear')
	for item in user_choice_var:
		print(user_choice_var, end='')




